<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    // get all pegawai
    public function index()
    {
        $pegawai = Pegawai::all();
        return response()->json($pegawai);
    }

    // save data master pegawai
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pegawai_nama' => 'required|max:100',
            'pegawai_nik' => 'required|max:16',
        ]);

        $pegawai = Pegawai::create($request->all());

        return response()->json($pegawai);
    }

    // update data master pegawai
    public function update(Request $request)
    {
        $validated = $request->validate([
            'pegawai_nama' => 'required|max:100',
            'pegawai_nik' => 'required|max:16',
        ]);

        $pegawai = Pegawai::findOrFail($request->id);
        $pegawai->update($request->all());

        return response()->json($pegawai);
    }

    // delete data master pegawai
    public function destroy($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->delete();

        return response()->json($pegawai);
    }
}
