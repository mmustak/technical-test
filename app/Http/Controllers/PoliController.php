<?php

namespace App\Http\Controllers;

use App\Models\Poli;
use Illuminate\Http\Request;

class PoliController extends Controller
{
    // get all poli
    public function index()
    {
        $poli = Poli::all();
        return response()->json($poli);
    }

    // save data master poli
    public function store(Request $request)
    {
        $validated = $request->validate([
            'poli_nama' => 'required|max:60',
            'poli_kode' => 'required|max:10',
        ]);

        $poli = Poli::create($request->all());

        return response()->json($poli);
    }

    // update data master poli
    public function update(Request $request)
    {
        $validated = $request->validate([
            'poli_nama' => 'required|max:60',
            'poli_kode' => 'required|max:10',
        ]);

        $poli = Poli::findOrFail($request->id);
        $poli->update($request->all());

        return response()->json($poli);
    }

    // delete data master poli
    public function destroy($id)
    {
        $poli = Poli::findOrFail($id);
        $poli->delete();

        return response()->json($poli);
    }
}
