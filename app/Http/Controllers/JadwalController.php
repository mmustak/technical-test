<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    // get all jadwal
    public function index()
    {
        $jadwal = Jadwal::with(['poli:id,poli_nama,poli_kode', 'pegawai:id,pegawai_nama,pegawai_nik'])->get();
        return response()->json($jadwal);
    }

    // save data jadwal praktek
    public function store(Request $request)
    {
        $validated = $request->validate([
            'poli_id' => 'required',
            'pegawai_id' => 'required',
            'hari' => ['required', 'in:SENIN,SELASA,RABU,KAMIS,JUMAT,SABTU,MINGGU'],
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
        ]);

        $jadwal = Jadwal::create($request->all());

        return response()->json($jadwal);
    }

    // update data jadwal praktek
    public function update(Request $request)
    {
        $validated = $request->validate([
            'poli_id' => 'required',
            'pegawai_id' => 'required',
            'hari' => ['required', 'in:SENIN,SELASA,RABU,KAMIS,JUMAT,SABTU,MINGGU'],
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
        ]);

        $jadwal = Jadwal::findOrFail($request->id);
        $jadwal->update($request->all());

        return response()->json($jadwal);
    }

    // delete data jadwal praktek
    public function destroy($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->delete();

        return response()->json($jadwal);
    }
}
