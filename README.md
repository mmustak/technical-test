# Simple API with Laravel

## Menjalankan aplikasi

```
composer install
php artisan migrate
php artisan serve
```

Jangan lupa untuk membuat file `.env` untuk koneksi ke Database

## Dokumentasi API

Berikut adalah Dokumentasi REST API yang bisa diakses melalui `http://localhost:8000/api`

### `GET`

- `/pegawai` : mengambil semua data pegawai
- `/poli` : mengambil semua data poli
- `/jadwal` : mengambil semua data jadwal praktek

### `POST`

- `/pegawai` : membuat data pegawai
  - Body :
    - `pegawai_nama: String` (required): nama pegawai
    - `pegawai_nik: String` (required): nomor induk kepegawaian
- `/poli` : membuat data poli
  - Body :
    - `poli_nama: String` (required): nama poli
    - `poli_kode: String` (required): kode poli
- `/jadwal` : membuat data jadwal praktek
  - Body :
    - `poli_id: Integer` (required): id poli
    - `pegawai_id: Integer` (required): id pegawai
    - `hari: String` (required): hari [SENIN, SELASA, RABU, KAMIS, JUMAT, SABTU, MINGGU]
    - `jam_mulai: String` (required): jam mulai praktek (contoh: "08:00:00")
    - `jam_selesai: String` (required): jam selesai praktek (contoh: "17:00:00")

### `PUT`

- `/pegawai` : ubah data pegawai
  - Body :
    - `id: Integer` (required): id pegawai
    - `pegawai_nama: String` (required): nama pegawai
    - `pegawai_nik: String` (required): nomor induk kepegawaian
- `/poli` : ubah data poli
  - Body :
    - `id: Integer` (required): id poli
    - `poli_nama: String` (required): nama poli
    - `poli_kode: String` (required): kode poli
- `/jadwal` : ubah data jadwal praktek
  - Body :
    - `id: Integer` (required): id jadwal praktek
    - `poli_id: Integer` (required): id poli
    - `pegawai_id: Integer` (required): id pegawai
    - `hari: String` (required): hari [SENIN, SELASA, RABU, KAMIS, JUMAT, SABTU, MINGGU]
    - `jam_mulai: String` (required): jam mulai praktek (contoh: "08:00:00")
    - `jam_selesai: String` (required): jam selesai praktek (contoh: "17:00:00")

### `DELETE`

- `/pegawai/{id}` : menghapus data pegawai berdasarkan id pegawai
- `/poli/{id}` : menghapus data poli berdasarkan id poli
- `/jadwal/{id}` : menghapus data jadwal praktek berdasarkan id jadwal praktek